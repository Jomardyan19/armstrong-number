﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp109
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Your Number = ");
            int number = Convert.ToInt32(Console.ReadLine());
            IsArmstrongNumber(number);
        }
        public static void IsArmstrongNumber(int number)
        { 
            string num = number.ToString();
            double sum = 0;
            foreach (var x in num)
            {
                sum += Math.Pow(Convert.ToDouble(x.ToString()), num.Length);
            }
            if (sum == number)
            {
                Console.WriteLine($"{number} is Armstrong Number");
            }
            else
            {
                Console.WriteLine($"{number} is not Armstrong Number");
            }
            
        }
    }
}
